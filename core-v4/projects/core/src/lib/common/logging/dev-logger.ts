import { isDevMode } from '@angular/core';


/**
 * console.log() wrapper.
 * Logging is enabled in dev mode only.
 */
export namespace DevLogger {

  /**
   * Returns true if logging is enabled.
   *   (For now, DevLogger logging is enabled if the app is running in dev mode.)
   */
  export function isLoggable(): boolean {
    return isDevMode();
  }

  /**
   * Logs the message if the app is running in dev mode.
   * Uses console.log().
   * 
   * @param message log message string or object.
   */
  export function log(message: string);
  export function log(message: string, sub1: (any | null));
  export function log(message: string, sub1: (any | null), sub2: (any | null));
  export function log(obj0: any);
  export function log(obj0: any, obj1: (any | null));
  export function log(obj0: any, obj1: (any | null), obj2: (any | null));
  export function log(arg0: (any | string), arg1: (any | null) = null, arg2: (any | null) = null) {
    if (isDevMode()) {
      if (typeof arg0 === 'string') {
        let msg = arg0 as string;
        if (arg2 != null) {
          console.log(msg, arg1, arg2);
        } else if (arg1 != null) {
          console.log(msg, arg1);
        } else {
          console.log(msg);
        }
      } else {
        if (typeof arg0 === 'object') {
          let message = '';
          try {
            if (arg0) {
              message = JSON.parse(JSON.stringify(arg0));
              if (arg1) {
                message += ' ' + JSON.parse(JSON.stringify(arg1));
                if (arg2) {
                  message += ' ' + JSON.parse(JSON.stringify(arg2));
                }
              }
            }
          } catch (ex) { /* ignore */ }
          console.log(message);
        } else {
          console.log(arg0, arg1, arg2);
        }
      }
    }
  }

  /**
   * Logs the message if the app is running in dev mode.
   * Uses console.info().
   * 
   * @param message info message string or object.
   */
  export function info(message: string);
  export function info(message: string, sub1: (any | null));
  export function info(message: string, sub1: (any | null), sub2: (any | null));
  export function info(obj0: any);
  export function info(obj0: any, obj1: (any | null));
  export function info(obj0: any, obj1: (any | null), obj2: (any | null));
  export function info(arg0: (any | string), arg1: (any | null) = null, arg2: (any | null) = null) {
    if (isDevMode()) {
      if (typeof arg0 === 'string') {
        let msg = arg0 as string;
        if (arg2 != null) {
          console.info(msg, arg1, arg2);
        } else if (arg1 != null) {
          console.info(msg, arg1);
        } else {
          console.info(msg);
        }
      } else {
        if (typeof arg0 === 'object') {
          let message = '';
          try {
            if (arg0) {
              message = JSON.parse(JSON.stringify(arg0));
              if (arg1) {
                message += ' ' + JSON.parse(JSON.stringify(arg1));
                if (arg2) {
                  message += ' ' + JSON.parse(JSON.stringify(arg2));
                }
              }
            }
          } catch (ex) { /* ignore */ }
          console.info(message);
        } else {
          console.info(arg0, arg1, arg2);
        }
      }
    }
  }

  /**
   * Logs the warning message using console.warn().
   * 
   * @param message warn message string or object.
   */
  export function warn(message: string);
  export function warn(message: string, sub1: (any | null));
  export function warn(message: string, sub1: (any | null), sub2: (any | null));
  export function warn(obj0: any);
  export function warn(obj0: any, obj1: (any | null));
  export function warn(obj0: any, obj1: (any | null), obj2: (any | null));
  export function warn(arg0: (any | string), arg1: (any | null) = null, arg2: (any | null) = null) {
    // if (isDevMode()) {
      if (typeof arg0 === 'string') {
        let msg = arg0 as string;
        if (arg2 != null) {
          console.warn(msg, arg1, arg2);
        } else if (arg1 != null) {
          console.warn(msg, arg1);
        } else {
          console.warn(msg);
        }
      } else {
        if (typeof arg0 === 'object') {
          let message = '';
          try {
            if (arg0) {
              message = JSON.parse(JSON.stringify(arg0));
              if (arg1) {
                message += ' ' + JSON.parse(JSON.stringify(arg1));
                if (arg2) {
                  message += ' ' + JSON.parse(JSON.stringify(arg2));
                }
              }
            }
          } catch (ex) { /* ignore */ }
          console.warn(message);
        } else {
          console.warn(arg0, arg1, arg2);
        }
      }
    // }
  }

  /**
   * Logs the error message using console.error().
   * 
   * @param message error message string or object.
   */
  export function error(message: string);
  export function error(message: string, sub1: (any | null));
  export function error(message: string, sub1: (any | null), sub2: (any | null));
  export function error(obj0: any);
  export function error(obj0: any, obj1: (any | null));
  export function error(obj0: any, obj1: (any | null), obj2: (any | null));
  export function error(arg0: (any | string), arg1: (any | null) = null, arg2: (any | null) = null) {
    // if (isDevMode()) {
      if (typeof arg0 === 'string') {
        let msg = arg0 as string;
        if (arg2 != null) {
          console.error(msg, arg1, arg2);
        } else if (arg1 != null) {
          console.error(msg, arg1);
        } else {
          console.error(msg);
        }
      } else {
        if (typeof arg0 === 'object') {
          let message = '';
          try {
            if (arg0) {
              message = JSON.parse(JSON.stringify(arg0));
              if (arg1) {
                message += ' ' + JSON.parse(JSON.stringify(arg1));
                if (arg2) {
                  message += ' ' + JSON.parse(JSON.stringify(arg2));
                }
              }
            }
          } catch (ex) { /* ignore */ }
          console.error(message);
        } else {
          console.error(arg0, arg1, arg2);
        }
      }
    // }
  }

}
