import { async, inject, TestBed } from '@angular/core/testing';
import {} from 'jasmine';

import { UniqueIdUtil } from './unique-id-util';


// https://angular.io/docs/ts/latest/guide/testing.html

describe('UniqueIdUtil id()', () => {

  // Note;
  // The current implementation of UniqueIdUtil does not guarantee neither expectation to be true.
  // --> TBD: Need a better implementation.

  it ('Unique ids should be unique.', () => {
    let id1 = UniqueIdUtil.id();
    let id2 = UniqueIdUtil.id();
    expect(id1 != id2).toBeTruthy();
  });

  it ('Unique id should monotonically increase.', () => {
    let id1 = UniqueIdUtil.id();
    let id2 = UniqueIdUtil.id();
    expect(id1 < id2).toBeTruthy();
  });

});
