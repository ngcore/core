import { async, inject, TestBed } from '@angular/core/testing';
// import { IonicModule } from 'ionic-angular';
import {} from 'jasmine';

import { RandomNumberUtil } from './random-number-util';


// https://angular.io/docs/ts/latest/guide/testing.html

describe('RandomNumberUtil randomIndex()', () => {
  let numberList: number[];
  let stringList: string[];
  let numberListLength: number;
  let stringListLength: number;

  beforeEach(async(() => {
    numberList = [1,2,3,4];
    numberListLength = numberList.length;
    stringList = ["aa", "bb", "cc"];
    stringListLength = stringList.length;
  }));

  it ('should return a random number in a proper range.', () => {
    expect(RandomNumberUtil.randomIndex(numberList)).toBeGreaterThanOrEqual(0);
    expect(RandomNumberUtil.randomIndex(numberList)).toBeLessThan(numberListLength);
    expect(RandomNumberUtil.randomIndex(stringList)).toBeGreaterThanOrEqual(0);
    expect(RandomNumberUtil.randomIndex(stringList)).toBeLessThan(stringListLength);
  });

});


describe('RandomNumberUtil randomElement()', () => {
  let numberList: number[];
  let stringList: string[];
  let numberListLength: number;
  let stringListLength: number;

  beforeEach(async(() => {
    numberList = [1,2,3,4];
    numberListLength = numberList.length;
    stringList = ["aa", "bb", "cc"];
    stringListLength = stringList.length;
  }));

  it ('should return a valid random element.', () => {
    expect(RandomNumberUtil.randomElement(numberList)).not.toBeNull();
    expect(RandomNumberUtil.randomElement(stringList)).not.toBeNull();
  });

});
