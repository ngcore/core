import { RGBColor } from '../core/rgb-color';


/**
 * Util functions for RGBColor.
 */
export namespace RGBColorUtil {

  export function randomColor() : RGBColor {
    let r = Math.floor(Math.random() * 256);
    let g = Math.floor(Math.random() * 256);
    let b = Math.floor(Math.random() * 256);
    let rgb = new RGBColor(r, g, b);
    return rgb;
  }

}
