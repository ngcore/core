/**
 * Util functions for random ID generation.
 */
// TBD: Need a better way to generate unique IDs.
// (Cf. UniqueIdUtil.)
export namespace RandomIdUtil {

  // For now, we just use a random number.
  // --> It can be converted to long on the server side.
  export function id(): string {
    return numberId().toString();
    // return stringId();
  }

  // [MIN, MAX).
  //  --> 12 digits
  const MIN_NUMBER_ID = 100000000000;
  const MAX_NUMBER_ID = 1000000000000;
  export function numberId(): number {
    let r = MIN_NUMBER_ID + Math.floor(Math.random() * (MAX_NUMBER_ID - MIN_NUMBER_ID));
    return r;
  }

  // tbd: Use uuid or just number, or even alphanumeric string?
  export function stringId(): string {
    // let id = uuid();
    // let id = numberId().toString();
    let id = randomString();
    return id;
  }

  export function uuid(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  const RANDOM_STRING_LENGTH: number = 12;
  const _ALPHA_NUMERICS: string[] = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    ];
  const _ALPHANUM_ARRAY_SIZE: number = _ALPHA_NUMERICS.length;
  function randomString(len: number = RANDOM_STRING_LENGTH): string {
    var str = '';
    for (let i = 0; i < len; i++) {
      let r = Math.floor(Math.random() * _ALPHANUM_ARRAY_SIZE);
      str += _ALPHA_NUMERICS[r];
    }
    return str;
  }

}
