import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;
import { DateRange } from '../core/date-range';
import { DateTimeUtil } from './date-time-util';


/**
 * DateId: yyyymmdd
 *    (Month: 01 ~ 12.)
 * tbd: support other formats like "yyyy/mm/dd" or "yyyy-mm-dd"  as well?
 * ("yyyymmdd" format is good because it can be directly cast to number, e.g., for comparison, etc.)
 */
export namespace DateIdUtil {

  // Note:
  // Due to the daylight saving time,
  // A day can be 25 hours or 23 hours (each ocurring once per year).
  // Because of this we add (3600 * 1000 + 10) (1 hour + epsilon) when we do data arithmetic...

  // tbd.
  export function isDateIdValid(dateId: string): boolean {
    return false;
  }

  // tbd:
  export function convertToDate(dateId: string): Date {
    // tbd:
    // assert dateId.length == 8
    let y = Number(dateId.substr(0, 4));
    let m = Number(dateId.substr(4, 2)) - 1;  // Note: -1
    let d = Number(dateId.substr(6, 2));
    let date = new Date(y, m, d);
    return date;
  }

  // Note that hours/minutes/seconds are ignored.
  export function convertToId(date: Date): string {
    let id: string = '';
    if (date) {
      id += date.getFullYear().toString();
      let m = date.getMonth() + 1;  // january == 0.
      id += (m < 10) ? '0' + m : m.toString();
      let d = date.getDate();
      id += (d < 10) ? '0' + d : d.toString();
    }
    return id;
  }

  /**
   * Returns today's 'dateId' (yyyymmdd).
   */
  export function getTodayId(): string {
    let today = new Date();
    let id = convertToId(today);
    return id;
  }

  export function getTomorrowId(): string {
    // let tomorrow = new Date(DateTimeUtil.getMidnight(new Date().getTime()) + 24 * 3600 * 1000 + (3600 * 1000 + 10));
    // // if(isDL()) dl.log("------------ tomorrow = " + tomorrow);
    // let id = convertToId(tomorrow);
    // // if(isDL()) dl.log("------------ tomorrow.id = " + id);
    // return id;
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    return convertToId(tomorrow);
  }

  export function getYesterdayId(): string {
    // let yesterday = new Date(DateTimeUtil.getMidnight(new Date().getTime()) - 24 * 3600 * 1000 + (3600 * 1000 + 10));
    // // if(isDL()) dl.log("------------ yesterday = " + yesterday);
    // let id = convertToId(yesterday);
    // // if(isDL()) dl.log("------------ yesterday.id = " + id);
    // return id;
    let yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    return convertToId(yesterday);
  }

  export function getNextDayId(dateId: string): string {
    // let date = convertToDate(dateId);
    // let nextDay = new Date(date.getTime() + 24 * 3600 * 1000);
    // let id = convertToId(nextDay);
    // return id;
    return getNthDayId(dateId, 1);
  }

  // Supports both positive and negative dayN.
  export function getNthDayId(dateId: string, dayN: number = 1): string {
    let date = convertToDate(dateId);
    // Note: 1 hour added to accomodate daylight saving time changes.
    // TBD: Can this be an issue in certain use cases????
    let nthDay = new Date(date.getTime() + dayN * 24 * 3600 * 1000 + (3600 * 1000 + 1010));
    let id = convertToId(nthDay);

    // testing
    // if(isDL()) dl.log(`### dateId = ${dateId}; dayN = ${dayN}; date = ${date}; nthDay = ${nthDay}; id = ${id}. `)
    // testing

    return id;
  }

  export function convertToDateRange(dateId: string): DateRange {
    let nextDateId = convertToId(convertToDate(dateId));
    let range = new DateRange(dateId, nextDateId);
    return range;
  }


  // ISO 8601 foramt
  export function getISODateString(dateId: (string | null), excludeTimePart: boolean = false): string {
    let str: string = "";
    if (dateId) {
      let date = convertToDate(dateId);
      // let str = date.toDateString();

      // temporary
      // let locale = "en-us";
      let locale = undefined;
      str = date.toISOString();
      if(excludeTimePart) {
        str = str.substring(0, 10);
      }
      // if(isDL()) dl.log(">>>>>>> dateId = " + dateId + "; date = " + date + "; str = " + str);
    }
    return str;
  }
  export function fromISODateString(str: string): string {
    let isoDate = new Date(str);
    let id = convertToId(isoDate);
    return id;
  }

  // Locale-dependent string ???
  export function getLocaleDateString(dateId: (string | null)): string {
    let str: string = "";
    if (dateId) {
      let date = convertToDate(dateId);
      // let str = date.toDateString();

      // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
      // temporary
      // let locale = "en-us";
      let locale = undefined;
      // str = date.toLocaleString(locale, { year: 'numeric', month: 'long', day: 'numeric' });
      str = date.toLocaleString(locale, { month: 'long', day: 'numeric', weekday: 'short' });
      // if(isDL()) dl.log(">>>>>>> dateId = " + dateId + "; date = " + date + "; str = " + str);
    }
    return str;
  }
  export function getDateString(dateId: (string | null), sep: (string | null) = null): string {
    // tbd:
    // assert dateId.length == 8
    if (!dateId || dateId.length < 8) {  // ??
      return "";
    }

    if(sep == null) {  // '' is valid separator.
      sep = '/';
    }

    // let date = convertToDate(dateId);
    // let str = date.toDateString();

    // TBD: order ???
    // Note that by using number, we are removing leading 0's, if any.
    let y = Number(dateId.substr(0, 4));
    let m = Number(dateId.substr(4, 2));   // No -1.
    let d = Number(dateId.substr(6, 2));
    let str = y + sep + m + sep + d;
    return str;
  }

  // tbd:
  export function convertToEpochMillis(dateId: string): number {
    // tbd: Use DateTimeUtil ???
    return convertToDate(dateId).getTime();
  }
  export function convertFromEpochMillis(millis: number): string {
    let date = new Date(millis);
    return convertToId(date);  // It effectively takes "floors";
  }


  /**
   * Returns Sunday id of the given week.
   *
   * @param dateId Reference dateId. Today is used by default.
   */
  export function getSundayOfWeek(dateId: string = DateIdUtil.getTodayId()): string {
    return convertToId(DateTimeUtil.getSundayMidnightDate(convertToDate(dateId)));
  }

  /**
   * Returns the id of the first day of the given month.
   *
   * @param dateId Reference dateId. Today is used by default.
   */
  export function getFirstDayOfMonth(dateId: string = DateIdUtil.getTodayId()): string {
    // return convertToId(DateTimeUtil.getFirstDayMidnightDate(convertToDate(dateId)));
    return dateId.substr(0, 6) + '01';
  }

  /**
   * Returns the number of days until the same day next month.
   *
   * @param dateId Reference dateId. Today is used by default.
   */
  export function getNumberOfDaysForMonth(dateId: string = DateIdUtil.getTodayId()): number {
    return DateTimeUtil.getNumberOfDaysForMonth(convertToEpochMillis(dateId));
  }

  /**
   * Returns the number of days from the same day in the previous month.
   *
   * @param dateId Reference dateId. Today is used by default.
   */
  export function getNumberOfDaysForPreviousMonth(dateId: string = DateIdUtil.getTodayId()): number {
    return DateTimeUtil.getNumberOfDaysForPreviousMonth(convertToEpochMillis(dateId));
  }

  /**
   * Returns the number of days between the same day next month and the same day the following month.
   *
   * @param dateId Reference dateId. Today is used by default.
   */
  export function getNumberOfDaysForNextMonth(dateId: string = DateIdUtil.getTodayId()): number {
    return DateTimeUtil.getNumberOfDaysForNextMonth(convertToEpochMillis(dateId));
  }


}
