import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;
import { RGBColor } from '../core/rgb-color';
import { ColorPalette } from '../core/color-palette';
import { ColorTag } from '../core/color-tag';


/**
 * Color tag is a list of 9 colors (arranged in a square).
 */
export namespace ColorTagUtil {

  export function randomColorTag() : ColorTag {

    // [1] Starting from the first cell and go through rows....
    // // Avoid the same colors for adjacent cells.
    // let idx0 = ColorPalette.randomIndex();
    // let idx1 = ColorPalette.randomIndex([idx0]);
    // let idx2 = ColorPalette.randomIndex([idx1]);
    // let idx3 = ColorPalette.randomIndex([idx0]);
    // let idx4 = ColorPalette.randomIndex([idx1,idx3]);
    // let idx5 = ColorPalette.randomIndex([idx2,idx4]);
    // let idx6 = ColorPalette.randomIndex([idx3]);
    // let idx7 = ColorPalette.randomIndex([idx4,idx6]);
    // let idx8 = ColorPalette.randomIndex([idx5,idx7]);

    // let indexes: number[] = [
    //   idx0, idx1, idx2,
    //   idx3, idx4, idx5,
    //   idx6, idx7, idx8
    //   // ColorPalette.randomIndex(), ColorPalette.randomIndex(), ColorPalette.randomIndex(),
    //   // ColorPalette.randomIndex(), ColorPalette.randomIndex(), ColorPalette.randomIndex(),
    //   // ColorPalette.randomIndex(), ColorPalette.randomIndex(), ColorPalette.randomIndex()
    // ];
    // // if(isDL()) dl.log('>>> randomColorTag(): indexes = ' + indexes.toString());


    // [2] Starting from the center.
    // Avoid the same colors for adjacent cells.
    // let idx4 = ColorPalette.randomIndex();
    let idx4 = ColorPalette.randomIndex([0, 1, 2, 3]);   // Exclude gray colors from the center
    let idx0 = ColorPalette.randomWeightedIndex(idx4);
    let idx1 = ColorPalette.randomWeightedIndex(idx4, [idx0, idx4]);
    let idx2 = ColorPalette.randomWeightedIndex(idx4, [idx1]);
    let idx3 = ColorPalette.randomWeightedIndex(idx4, [idx0, idx4]);
    let idx5 = ColorPalette.randomWeightedIndex(idx4, [idx2, idx4]);
    let idx6 = ColorPalette.randomWeightedIndex(idx4, [idx3]);
    let idx7 = ColorPalette.randomWeightedIndex(idx4, [idx4, idx6]);
    let idx8 = ColorPalette.randomWeightedIndex(idx4, [idx5, idx7]);

    let indexes: number[] = [
      idx0, idx1, idx2,
      idx3, idx4, idx5,
      idx6, idx7, idx8
    ];
    // if(isDL()) dl.log('>>> randomColorTag(): indexes = ' + indexes.toString());



    let tag = new ColorTag(indexes);
    // if(isDL()) dl.log('>>> randomColorTag(): tag = ' + tag.toString());

    return tag;
  }

}
