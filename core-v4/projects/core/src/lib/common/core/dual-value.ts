/**
 * Represents a pair of number, lower to upper.
 */
export interface DualValue {
  lower: number;
  upper: number;
}
