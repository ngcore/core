import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;

export namespace UrlUtil {

  /**
   * Returns true if the given arg is an absolute url.
   * @param url URL string to test
   */
  export function isAbsolute(url: string): boolean {
    if(!url) {
      return false;
    }
    return UrlUtil.isAbsoluteUrl(url);
  }

  /**
   * Returns true if the given arg is not an absolute url.
   * @param url URL string to test
   */
  export function isRelative(url: string): boolean {
    if(url == null) {
      return false;
    }
    return ! UrlUtil.isAbsoluteUrl(url);
  }

  // We only check http, https, and protocol relative URLs.
  export function isAbsoluteUrl(url: string): boolean {
    let trimmed = url.trim();
    let pat = /^https?:\/\/|\/\//i;
    return pat.test(trimmed);
  }

}
