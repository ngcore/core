import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;
import { RGBColor } from './rgb-color';


/**
 * GrayscaleColor is grayscale-converted color, not a grayscale RGBColor.
 * (e.g., RGBColor(0,0,0) is grayscale, but not GrayscaleColor in our definition.)
 */
// 
// 
export class GrayscaleColor extends RGBColor {
  
  // Note that the ctor takes the color RGB components not the grayscale-converted RGB.
  // 3 numbers [0, 255]: R, G, B in color space.
  constructor(private colorR: number, private colorG: number, private colorB: number, doNotConvert = false) {
    // super();
    super(colorR, colorG, colorB);
    if(doNotConvert === false) {
      let Y = this.getLuminaceComponent();
      this.R = Y;
      this.G = Y;
      this.B = Y;
    }
    // if(isDL()) dl.log("GrayscaleColor() R,G,B = " + this.R + "," + this.G + "," + this.B);
  }
  private getLuminaceComponent(): number {
    return GrayscaleColor.convertToLuminanceComponent(this.colorR, this.colorG, this.colorB);
  }

  static createGrayscaleColor(R: number, G: number, B: number): GrayscaleColor {
    // return new GrayscaleColor(R, G, B);
    let Y = GrayscaleColor.convertToLuminanceComponent(R, G, B);
    return new GrayscaleColor(Y, Y, Y, true);
  }
  static convertToGrayscale(color: RGBColor): GrayscaleColor {
    return GrayscaleColor.createGrayscaleColor(color.R, color.G, color.B);
  }

  // https://en.wikipedia.org/wiki/Grayscale
  static convertToLuminanceComponent(R: number, G: number, B: number): number {

    let R_lin = GrayscaleColor.computeGammaExpandedColor(R/255);
    let G_lin = GrayscaleColor.computeGammaExpandedColor(G/255);
    let B_lin = GrayscaleColor.computeGammaExpandedColor(B/255);

    let Y_lin = 0.2126 * R_lin + 0.7152 * G_lin + 0.0722 * B_lin;
    let Y_rgb = GrayscaleColor.computeGammaCompressedLuminance(Y_lin);
    let Y = (Y_rgb < 0) ? 0 : ((Y_rgb > 1) ? 255 : Math.floor(Y_rgb * 255));

    return Y;
  }

  private static computeGammaExpandedColor(C_rgb: number) {
    let C_lin: number;
    if(C_rgb <= 0.04045) {
      C_lin = C_rgb / 12.92;
    } else {
      C_lin = Math.pow(((C_rgb + 0.055) / 1.055), 2.4);
    }
    return C_lin;
  }

  private static computeGammaCompressedLuminance(Y_lin: number) {
    let Y_rgb: number;
    if(Y_lin <= 0.0031308) {
      Y_rgb = 12.92 * Y_lin;
    } else {
      Y_rgb = 1.055 * Math.pow(Y_lin, 1/2.4) - 0.055;
    }
    return Y_rgb;
  }

}
