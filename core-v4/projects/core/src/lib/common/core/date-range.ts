// import { DualValue } from './dual-value';

/**
 * Represents a date range, from startDate to endDate.
 * Note that a date range can be open-ended on either side.
 */
export class DateRange // implements DualValue
{
  // // [ : )
  // // date: yyyymmdd
  // // Note that date range can be open-ended on either side.
  // constructor(public startDate: string = null, public endDate: string = null) {
  // }

  public startDate: (string | null) = null;
  public endDate: (string | null) = null;

  public get dayCount(): number {
    if ((!this.startDate) || (!this.endDate)) {
      return -1;
    } else {
      // let d = parseInt(this.endDate) - parseInt(this.startDate);
      // return d;

      let y1 = Number(this.startDate.substr(0, 4));
      let m1 = Number(this.startDate.substr(4, 2)) - 1;  // Note: -1
      let d1 = Number(this.startDate.substr(6, 2));
      let date1 = new Date(y1, m1, d1);

      let y2 = Number(this.endDate.substr(0, 4));
      let m2 = Number(this.endDate.substr(4, 2)) - 1;  // Note: -1
      let d2 = Number(this.endDate.substr(6, 2));
      let date2 = new Date(y2, m2, d2);

      let millis = date2.getTime() - date1.getTime();
      var diffDays = Math.ceil(millis / (24 * 3600 * 1000));

      return diffDays;
    }
  }

  constructor();
  constructor(_startDate: string);
  constructor(_startDate: string, _endDate: string);
  constructor(_startDate: string, _dayCount: number);
  constructor(arg1?: string, arg2?: string | number) {
    if (!arg1 && !arg2) {
      this.startDate = null;
      this.endDate = null;
    } else {
      if ((typeof arg1) == 'string') {
        // tbd: Validate? yyyymmdd.
        this.startDate = arg1 as string;
        if ((typeof arg2) == 'string') {
          // tbd: Validate? 
          //      yyyymmdd
          //      startDate < endDate
          this.endDate = arg2 as string;
        } else if ((typeof arg2) == 'number') {
          // tbd: Validate? dayCount > 0
          let dayCount = arg2 as number;

          // TBD:
          // Copy of DateIdUtil.getNthDayId() implementation.
          // Need to refactor....

          let y1 = Number(this.startDate.substr(0, 4));
          let m1 = Number(this.startDate.substr(4, 2)) - 1;  // Note: -1
          let d1 = Number(this.startDate.substr(6, 2));
          let date = new Date(y1, m1, d1);

          let nthDay = new Date(date.getTime() + dayCount * 24 * 3600 * 1000);
          let id: string = nthDay.getFullYear().toString();
          let m2 = nthDay.getMonth() + 1;  // january == 0.
          id += (m2 < 10) ? '0' + m2 : m2.toString();
          let d2 = nthDay.getDate();
          id += (d2 < 10) ? '0' + d2 : d2.toString();

          this.endDate = id;
        } else {
          // ????
        }
      } else {
        // ????
      }
    }
  }


  // Moved to DateIdUtil.
  // // TBD: Single-day date range???
  // static convertToRange(date: string): DateRange {
  //   let range = new DateRange(date, date + 1);    // TBD: What about the end of month????
  //   return range;
  // }

  // TBD:
  // Get a list/array of date ids from date range???
  // ...


  // Note that in order for these to work,
  // the dates all should be in the same formate.
  // In particular, we only support "Date ID" formats.
  isInside(dateId: string): boolean {
    return (((!this.startDate) || dateId >= this.startDate) && ((!this.endDate) || dateId < this.endDate));
  }
  isOutside(dateId: string): boolean {
    return (((this.startDate != null) && dateId < this.startDate) || ((this.endDate != null) && dateId >= this.endDate));
  }

  toString(): string {
    return '(' + this.startDate + '-' + this.endDate + ')';
  }

  clone(): DateRange {
    let cloned = Object.assign(new DateRange(), this) as DateRange;
    return cloned;
  }
  static clone(obj: any): DateRange {
    let cloned = Object.assign(new DateRange(), obj) as DateRange;
    return cloned;
  }

}
