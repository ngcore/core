import { throwError as observableThrowError, Observable } from 'rxjs';

import { catchError, map } from 'rxjs/operators';
import { Injectable, isDevMode, Inject, Optional } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;
import { UrlUtil } from '../core/url-util';


/**
 * Global application config.
 * AppConfig needs to be initialized via APP_INITIALIZER.
 * For example,
 * providers: [
 *   { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => config.load(), deps: [AppConfig], multi: true },
 * ]
 */
@Injectable({
  providedIn : 'root'
})
export class AppConfig {

  // hard-coded, for now.
  private static CONFIG_FOLDER = 'configs/';
  private static CONFIG_FILE_PROD = AppConfig.CONFIG_FOLDER + 'app-config.json';
  private static CONFIG_FILE_DEV = AppConfig.CONFIG_FOLDER + 'app-config.dev.json';

  private fileName: (string | null) = null;
  private _config: { [key: string]: any } = {};

  private _isLoaded: boolean = false;

  // constructor(private http: HttpClient) {  // ???
  constructor(
    @Optional() @Inject(APP_BASE_HREF) private origin: string,  // Needed for SSR
    private http: HttpClient,
  ) {
    if (!this.origin) {
      this.origin = ''; // ???
    }
  }

  public getConfigFile(): string {
    if (!this.fileName) {
      if (isDevMode()) {
        this.fileName = this.origin + AppConfig.CONFIG_FILE_DEV;
      } else {
        this.fileName = this.origin + AppConfig.CONFIG_FILE_PROD;
      }
    }
    // TBD: Use 'fallback' file? in case the config file does not exist?
    return this.fileName;
  }
  // Note: Config file can be absolute or relative URLs...
  public setConfigFile(_fileName: string): boolean {
    // if(this._isLoaded) {
    //   console.warn('Config already loaded.');
    //   return false;
    // }
    // this.fileName = _fileName;
    // return true;

    // For SSR (Angular Universal):
    // If the config file is given as a relative URL,
    // add the leading host url if the platform is 'server'.
    this.fileName = UrlUtil.isAbsolute(_fileName) ? _fileName : this.origin + _fileName;
    this._isLoaded = false;
    this._config = {};
    return true;
  }

  /**
   * Returns all config key-value pairs as a hash/object.
   */
  public get all(): { [key: string]: any } {
    return this._config;
  }

  // tbd:
  // For second level config values.
  public sub(key: string): Object {
    let s = this.all[key] as Object;
    return s;
  }

  /**
   * Returns the config value for the given key.
   */
  public get(key: string): (any | null) {
    // if(this._config) {
    return this._config[key];
    // } else {
    //   return null;
    // }
  }

  public getString(key: string, defaultVal: string = null): (string | null) {
    // return this.get(key) as string;
    if (key in this._config) {
      return this.get(key) as string;
    } else {
      return defaultVal;
    }
  }

  public getNumber(key: string, defaultVal: number = 0): number {
    // return this.get(key) as number;
    if (key in this._config) {
      return this.get(key) as number;
    } else {
      return defaultVal;
    }
  }
  // public getNumber(key: string, defaultVal: number = 0): number {
  //   let v = this.getString(key);
  //   if(v) {
  //     try {
  //       return parseInt(v);
  //     } catch(ex) {}
  //   }
  //   return defaultVal;
  // }

  // We use pretty strange logic.
  // if it's a string and if it's 'true', return true.
  // if it's a number and if it's not zero, return true.
  // otherwise, return false.
  public getBoolean(key: string, defaultVal: boolean = false): boolean {
    // return this.get(key) as boolean;
    if (key in this._config) {
      return this.get(key) as boolean;
    } else {
      return defaultVal;
    }
  }
  // public getBoolean(key: string): boolean {
  //   let v = this.getString(key);
  //   if(v) {
  //     let b = v.toLowerCase();
  //     if(b == 'true') {
  //       return true;
  //     } else {
  //       try {
  //         let n = parseInt(b);
  //         if(n != 0) {
  //           return true;
  //         }
  //       } catch(ex) {}
  //     }
  //   }
  //   return false;
  // }


  /**
   * Loads the config asynchronously.
   */
  public load() {
    // tbd
    const configFile = this.getConfigFile();
    // const configFile = 'https://xxx/configs/app-config.json';
    // tbd

    return new Promise((resolve, reject) => {
      let request = this.http.get(configFile);

      isDL() && dl.log('request = ', request);

      request.pipe(
        map(data => {
          isDL() && dl.log('data = ' + data);
          // let json = (data as any).json();
          // isDL() && dl.log('json = ' + json);
          // return json;
          return data || {};
        }),
        catchError((error: any) => {
          isDL() && dl.log('Error reading ' + configFile);
          isDL() && dl.log('error = ' + error);
          // resolve(error);
          reject(error);
          return observableThrowError(error.json().error || 'Server error');
        }))
        .subscribe((responseData) => {
          isDL() && dl.log('>>>>> responseData = ' + responseData);

          // tbd:
          // this.config = responseData;

          // ???
          this._config = {};
          for (let k in responseData) {
            this._config[k] = responseData[k];
          }

          // // temporary
          // for(let k in this._config) {
          //   if(isDL()) dl.log(`:::config::: key = ${k}; value = ${this._config[k]}`);
          // }

          this._isLoaded = true;
          resolve(true);
        });

      // request
      //   .subscribe(response => {
      //     isDL() && dl.log('response = ', response);
      //     let json = (response as any).json();
      //     isDL() && dl.log('json = ' + json);

      //     // this._config = json;
      //     // ???
      //     this._config = {};
      //     for (let k in json) {
      //       this._config[k] = json[k];
      //     }

      //     this._isLoaded = true;  // ???
      //     resolve(true);
      //   });
    });
  }
}
