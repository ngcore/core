
import { throwError as observableThrowError, Observable } from 'rxjs';

import { catchError, map } from 'rxjs/operators';
import { Injectable, isDevMode, Inject, Optional } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;


/**
 * Config object (read from JSON).
 */
// @Injectable()
export class CustomConfig {

  // hard-coded, for now.
  private static CONFIG_FOLDER = "configs/";
  private static CONFIG_FILE_SUFFIX = ".json";
  private static CONFIG_FILE_PROD = CustomConfig.CONFIG_FOLDER + "config" + CustomConfig.CONFIG_FILE_SUFFIX;
  private static CONFIG_FILE_DEV = CustomConfig.CONFIG_FOLDER + "config.dev" + CustomConfig.CONFIG_FILE_SUFFIX;

  private _isLoaded: boolean = false;
  private _config: { [key: string]: (Object | null) } = {};
  private _fileName: (string | null) = null;

  // constructor(private http: HttpClient) {  // ???
  constructor(
    @Optional() @Inject(APP_BASE_HREF) private origin: string,  // Needed for SSR
    private http: HttpClient   // TBD: Use HttpClient...
  ) {
  }

  private getConfigFile(): string {
    if (!this._fileName) {
      if (isDevMode()) {
        this._fileName = this.origin + CustomConfig.CONFIG_FILE_DEV;
      } else {
        this._fileName = this.origin + CustomConfig.CONFIG_FILE_PROD;
      }
    }
    return this._fileName;
  }

  // configFile: file name without ".json" suffix.
  // TBD: Support absolute URLs for custom config files.
  public loadConfig(configFile: (string | null)): Promise<boolean> {
    if (this._isLoaded) {
      throw new Error("Already loaded");
    } else {
      if (configFile) {
        if (isDevMode()) {
          this._fileName = this.origin + CustomConfig.CONFIG_FOLDER + configFile + ".dev" + CustomConfig.CONFIG_FILE_SUFFIX;
        } else {
          this._fileName = this.origin + CustomConfig.CONFIG_FOLDER + configFile + CustomConfig.CONFIG_FILE_SUFFIX;
        }
      }
      return this.load();
    }
  }

  /**
   * Use to get the data found in the second file (config file)
   */
  public get(key: string): (Object | null) {
    if (this._isLoaded) {
      return this._config[key];
    } else {
      return null;
    }
  }

  public getString(key: string): (string | null) {
    return this.get(key) as string;
  }

  public getNumber(key: string, defaultVal: number = 0): number {
    let v = this.getString(key);
    if (v) {
      try {
        return parseInt(v);
      } catch (ex) { }
    }
    return defaultVal;
  }

  // We use pretty strange logic.
  // if it's a string and if it's 'true', return true.
  // if it's a number and if it's not zero, return true.
  // otherwise, return false.
  public getBoolean(key: string): boolean {
    let v = this.getString(key);
    if (v) {
      let b = v.toLowerCase();
      if (b === 'true') {
        return true;
      } else {
        try {
          let n = parseInt(b);
          if (n !== 0) {
            return true;
          }
        } catch (ex) { }
      }
    }
    return false;
  }


  // TBD:
  // Need a better implementation to load configs.
  public load(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let request = this.http.get(this.getConfigFile());
      if (request) {  // can request be null???
        if (isDL()) dl.log("request = ", request);
        request
          .subscribe(response => {
            if (isDL()) dl.log("response = ", response);
            let json = (response as any).json();
            if (isDL()) dl.log("json = " + json);

            this._config = json;
            this._isLoaded = true;  // ???
            resolve(true);
          });
      } else {
        console.error('Config file is not valid: ' + this.getConfigFile());
        resolve(true);
      }
    });
  }
}
