import { Injectable, Injector, Inject, Optional } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;
import { CustomConfig } from './custom-config';
// import { isObservable } from '@angular/core/src/util/lang';


/**
 * Factory for building a custom-config object.
 */
@Injectable({
  providedIn : 'root'
})
export class ConfigFactory {

  constructor(
    @Optional() @Inject(APP_BASE_HREF) private origin: string,  // Needed for SSR
    // private injector: Injector,
    private http: HttpClient   // TBD: Use HttpClient...
  ) {
  }

  // configFile: file name without ".json" suffix.
  // public buildConfig(configFile: string): Observable<CustomConfig> {

  //   // ????
  //   return Observable.create((obs) => {
  //     // tbd: First implementation. Don't know if it even works....
  //     // Need a better/correct way to do this....
  //     let customConfig = new CustomConfig(this.http);
  //     let promise = customConfig.loadConfig(configFile);
  //     if(promise) {
  //       promise.then((suc: boolean) => {
  //         if(isDL()) dl.log("Build config suceeded.");
  //         return customConfig;
  //       }).catch((err) => {
  //         if(isDL()) dl.log("err = " + err);
  //       });
  //     }
  //   });

  // }

  // configFile: file name without ".json" suffix.
  public buildConfig(configFile: string): Observable<CustomConfig> {
    // tbd: First implementation. Don't know if it even works....
    // Need a better/correct way to do this....
    let customConfig = new CustomConfig(this.origin, this.http);
    let promise = customConfig.loadConfig(configFile);
    return Observable.create((observer: any) => {
      promise.then((suc) => {
        if(isDL()) dl.log("Build config suceeded.");
        observer.next(customConfig);
      }, (reason) => {
        observer.error(reason);
      })
    })
  }

}
