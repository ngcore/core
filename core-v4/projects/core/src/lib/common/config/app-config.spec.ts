import { async, inject, TestBed } from '@angular/core/testing';
import { } from 'jasmine';

import { AppConfig } from './app-config';
import { ConfigFactory } from './config-factory';
// import { HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';


// https://angular.io/docs/ts/latest/guide/testing.html

// ng test --include='**/config/*.spec.ts'


describe('AppConfig', () => {

  let service: AppConfig;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        // HttpClient,
        // HttpHandler,
        AppConfig,
        ConfigFactory,
      ]
    });

    service = TestBed.get(AppConfig);
    httpMock = TestBed.get(HttpTestingController);
  })

  afterEach(() => {
    httpMock.verify();
    TestBed.resetTestingModule();
  });

  // TBD
  it('tbd.', () => {
    service.load();

    httpMock.expectOne('/configs/app-config.json');

    expect(true == true).toBeTruthy();
  });


});
